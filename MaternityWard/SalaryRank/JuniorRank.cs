﻿namespace MaternityWard.SalaryRank
{
    public class JuniorRank : RankBase
    {
        public JuniorRank()
        {
            Salary = 1;
        }
    }
}
