﻿namespace MaternityWard.SalaryRank
{
    public class WorkerAtRisk : RankBase
    {
        public WorkerAtRisk(double percentagesRisk)
        {
            Salary = percentagesRisk;
        }
    }
}
