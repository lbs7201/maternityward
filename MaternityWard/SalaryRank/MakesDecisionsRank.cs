﻿namespace MaternityWard.SalaryRank
{
    public class MakesDecisionsRank : RankBase
    {
        public MakesDecisionsRank(double hourAmount)
        {
            if (hourAmount >= 50)
            {
                Salary = WagesRanks.PERCENTAGE_OF_WAGE_INCREASE_TO_MAKE_DECISIONS;
            }
            else
            {
                Salary = 1;
            }
        }
    }
}
