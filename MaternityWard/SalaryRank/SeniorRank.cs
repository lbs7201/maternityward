﻿namespace MaternityWard.SalaryRank
{
    public class SeniorRank : RankBase
    {
        public SeniorRank()
        {
            Salary = WagesRanks.PERCENTAGE_OF_WAGE_INCREASE_TO_SENIOR;
        }
    }
}
