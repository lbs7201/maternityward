﻿namespace MaternityWard.SalaryRank
{
    public class ExpertRank : RankBase
    {
        public ExpertRank()
        {
            Salary = WagesRanks.PERCENTAGE_OF_WAGE_INCREASE_TO_EXPERT;
        }
    }
}
