﻿namespace MaternityWard
{
    public struct WagesRanks
    {
        public const double HOURLY_MINIMOM_WAGE = 28.49; 
        public const double PERCENTAGE_OF_WAGE_INCREASE_TO_EXPERT = 0.3;
        public const double PERCENTAGE_OF_WAGE_INCREASE_TO_SENIOR = 0.05;
        public const double PERCENTAGE_OF_WAGE_INCREASE_TO_MAKE_DECISIONS = 0.5;
        public const double TWENTY_PERCENTAGE_OF_WAGE_INCREASE_TO_DEGREE_OF_RISK = 0.2;
        public const double ONE_HUNDERED_PERCENTAGE_OF_WAGE_INCREASE_TO_DEGREE_OF_RISK = 2.0;      
    }
}
