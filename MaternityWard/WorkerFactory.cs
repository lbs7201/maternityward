﻿using MaternityWard.Factories;
using MaternityWard.Factories.ProffesionalWorkersFactory;
using MaternityWard.WardRanks.Base;
using System;
using System.Collections.Generic;

namespace MaternityWard
{
    public class WorkerFactory : IWorkerFactory
    {
        Dictionary<string, IWorkerFactory> dictionaryOfWorkersType = new Dictionary<string, IWorkerFactory>()
            {
                { "Cleaner", new CleanerFactory() },
                { "CleanerToxicSubstances", new CleanerTToxicSubstancesFactory() },
                { "MultiCleaners", new MultiCleanersFactory() },
                { "ShiftManagerOfCleaners", new ShiftManagerOfCleanersFactory() },
                { "Chef", new ChefFactory() },
                { "Cooker", new CookerFactory() },
                { "DistributesFood", new DistributesFoodFactory() },
                { "SuChef", new SuChefFactory() },
                { "HeadOfDepartment", new HeadOfDepartmentFactory() },
                { "Doctor", new DoctorFactory() },
                { "SeniorDoctor", new SeniorDoctorFactory() },
                { "SpecializedDoctor", new SpecializedDoctorFactory() },
                { "Medic", new MedicFactory() },
                { "Paramedic", new ParamedicFactory() },
                { "BreechInternship", new BreechInternshipFactory() },
                { "InternshipDoctor", new InternshipDoctorFactory() },
                { "DepartmentManager", new DepartmentManagerFactory() },
                { "DeputyHeadOfDepartment", new DeputyHeadOfDepartmentFactory() },
                { "BreechMidwife", new BreechMidwifeFactory() },
                { "Midwife", new MidwifeFactory() },
                { "HeadNurse", new HeadNurseFactory() },
                { "Nurse", new NurseFactory() },
            };

        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            Console.WriteLine("Enter worker type");
            string workerType = Console.ReadLine();
            IWorkerFactory factory = dictionaryOfWorkersType[workerType];
            WorkerBase Worker = factory.CreateWorker(hourAmount, name);
            return Worker;
        }
    }
}
