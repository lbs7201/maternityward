﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class SpecializedDoctor : WorkerBase
    {
        public SpecializedDoctor(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
            Ranks.Add(new ExpertRank());
        }
    }
}
