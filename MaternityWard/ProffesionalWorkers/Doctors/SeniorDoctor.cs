﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    class SeniorDoctor : WorkerBase
    {
        public SeniorDoctor(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
        }
    }
}
