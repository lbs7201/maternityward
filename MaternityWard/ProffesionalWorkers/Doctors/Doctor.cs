﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class Doctor : WorkerBase
    {
        public Doctor(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
        }
    }
}
