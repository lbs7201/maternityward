﻿using MaternityWard.SalaryRank;

namespace MaternityWard.ProffesionalWorkers.Internship
{
    public class BreechInternship : WorkerBase
    {
        public BreechInternship(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
            Ranks.Add(new ExpertRank());
        }
    }
}
