﻿using MaternityWard.SalaryRank;

namespace MaternityWard.ProffesionalWorkers
{
    public class InternshipDoctor : WorkerBase
    {
        public InternshipDoctor(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
        }
    }
}
