﻿using MaternityWard.SalaryRank;

namespace MaternityWard.ProffesionalWorkers
{
    public class Paramedic : ProffesionalWorker
    {
        public Paramedic(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
        }
    }
}
