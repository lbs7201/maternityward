﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class Medic : WorkerBase
    {
        public Medic(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
        }
    }
}
