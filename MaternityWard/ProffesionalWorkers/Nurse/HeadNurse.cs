﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class HeadNurse : WorkerBase
    {
        public HeadNurse(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
        }
    }
}
