﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class Nurse : WorkerBase
    {
        public Nurse(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
        }
    }
}
