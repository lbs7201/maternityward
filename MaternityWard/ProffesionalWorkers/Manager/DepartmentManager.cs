﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class DepartmentManager : WorkerBase
    {
        public DepartmentManager(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new ManagerRank());
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
            Ranks.Add(new WorkerAtRisk(WagesRanks.ONE_HUNDERED_PERCENTAGE_OF_WAGE_INCREASE_TO_DEGREE_OF_RISK));
        }
    }
}
