﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    class DeputyHeadOfDepartment : WorkerBase
    {
        public DeputyHeadOfDepartment(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new ManagerRank());
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
        }
    }
}
