﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class BreechMidwife : WorkerBase
    {
        public BreechMidwife(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
            Ranks.Add(new ExpertRank());
        }
    }
}
