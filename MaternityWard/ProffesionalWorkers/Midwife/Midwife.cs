﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class Midwife : WorkerBase
    {
        public Midwife(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
            Ranks.Add(new SeniorRank());
        }
    }
}
