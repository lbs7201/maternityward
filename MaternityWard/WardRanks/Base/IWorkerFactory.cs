﻿namespace MaternityWard.WardRanks.Base
{
    interface IWorkerFactory
    {
        WorkerBase CreateWorker(double hourAmount, string name);
    }
}
