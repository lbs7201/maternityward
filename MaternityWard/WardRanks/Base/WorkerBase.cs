﻿using System.Collections.Generic;
using System.Linq;

namespace MaternityWard
{
    public abstract class WorkerBase
    {
        public string Name { get; set; }
        public double HoursAmount { get; set; }
        public List<RankBase> Ranks { get; set; }

        public WorkerBase()
        {
            this.Ranks = new List<RankBase>();
        }
        public virtual double CalculateSalary()
        {
            return HoursAmount * (WagesRanks.HOURLY_MINIMOM_WAGE + Ranks.Sum((rank) => rank.Salary));
        }
    }
}
