﻿using MaternityWard.WardRanks;
using System;
using System.Collections.Generic;
using System.Text;

namespace MaternityWard
{
    public class Worker
    {
        public string firstName { get; set; }
        public string lastName {get; set; }
        public DateTime StartOfShift { get; set; }
        public DateTime EndOfShift { get; set; }
    }
}
