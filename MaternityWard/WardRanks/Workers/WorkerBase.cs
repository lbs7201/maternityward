﻿using MaternityWard.SalaryRank;
using System;
using System.Collections.Generic;
using System.Text;

namespace MaternityWard
{
    public abstract class WorkerBase
    {
        public double HourAmount { get; set; }
        public List<RankBase> Ranks { get; set; }
        public virtual double Salary { get; set; }
    }
}
