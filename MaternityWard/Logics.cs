﻿using System;

namespace MaternityWard
{
    public class Logics 
    {
        int sum = 0; 
        Stock stock = new Stock();
        public WorkerBase InsertWorkerToProgram(string name, DateTime StartShift, DateTime EndShift)
        {
            double ShiftHoures = GetShiftHours(StartShift, EndShift);   
            WorkerBase createWorker = new WorkerFactory().CreateWorker(ShiftHoures, name);
            return createWorker; 
        }

        public double GetShiftHours(DateTime StartShift, DateTime EndShift)
        {
            TimeSpan shiftTime = EndShift - StartShift;
            double hours = shiftTime.Hours;
            double minutes = shiftTime.Minutes * 0.1;
            return hours + minutes;
        }

        public double GetMonthSalary(WorkerBase workerBase)
        {
            return workerBase.CalculateSalary();
        }

        public string GetUserInput(string message)
        {
            Console.WriteLine(message);
            return Console.ReadLine();
        }

        public DateTime GetDateTimeInput(string message)
        {
            string dateTime = GetUserInput(message);
            DateTime.TryParse(dateTime, out DateTime startDateTime);
            return startDateTime;
        }

        public void PrintAllWorkers()
        {
            foreach (var item in stock.workers)
            {
                sum++;
                string name = item.Name;
                var hoursAmount = item.HoursAmount;
                Console.WriteLine(sum + ") " + name + " - " + hoursAmount);
            }
        }

        public void EnterNewWorker()
        {
            string firstName = GetUserInput("Enter first name: ");
            string lastName = GetUserInput("Enter last name: ");
            string name = firstName + " " + lastName;
            DateTime startShift = GetDateTimeInput("Enter start shift: ");
            DateTime endShift = GetDateTimeInput("Enter end shift: ");
            WorkerBase worker = InsertWorkerToProgram(name, startShift, endShift);
            double MonthlySalary = GetMonthSalary(worker);
            Console.WriteLine(name + " your Salary is: " + MonthlySalary);
        }

        public void ChooseOption()
        {
            Console.WriteLine("Create new worker and enter his hours is shift enter 1");
            Console.WriteLine("get exist worker and enter his hours is shift enter 2");
            Console.WriteLine("get exist worker and show his salary enter 3");
            string Choose = Console.ReadLine();

            if (Choose == "1")
            {
                EnterNewWorker();
            }
            if (Choose == "2")
            {
                Console.WriteLine("Choose number of exist worker to enter hours shift");
                PrintAllWorkers(); 
                int chooseOption = int.Parse(Console.ReadLine());

                for (int i = 1; i < stock.workers.Count + 1; i++)
                {
                    if (chooseOption == i)
                    {
                        WorkerBase worker = stock.workers[i];
                        DateTime startShift = GetDateTimeInput("Enter start shift: ");
                        DateTime endShift = GetDateTimeInput("Enter end shift: ");
                        double hoursShift = GetShiftHours(startShift, endShift);
                        worker.HoursAmount += hoursShift;
                        Console.WriteLine(worker.CalculateSalary());
                    }
                }
            }
            if (Choose == "3")
            {
                Console.WriteLine("Choose number of exist worker to show his salary");
                PrintAllWorkers();
                int chooseOption = int.Parse(Console.ReadLine());
                for (int i = 1; i < stock.workers.Count + 1; i++)
                {
                    if (chooseOption == i)
                    {
                        WorkerBase worker = stock.workers[i];
                        Console.WriteLine(worker.CalculateSalary());
                    }
                }
            }
        }
    }
}
