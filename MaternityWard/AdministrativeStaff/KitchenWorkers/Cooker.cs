﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff.KitchenWorkers
{
    public class Cooker : WorkerBase
    {
        public Cooker(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
        }
    }
}
