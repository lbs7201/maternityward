﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff.KitchenWorkers
{
    public class SuChef : WorkerBase
    {
        public SuChef(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
            Ranks.Add(new ExpertRank());
        }
    }
}
