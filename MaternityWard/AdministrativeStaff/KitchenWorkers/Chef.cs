﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff.KitchenWorkers
{
    public class Chef : WorkerBase
    {
        public Chef(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
            Ranks.Add(new ExpertRank());
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
        }
    }
}
