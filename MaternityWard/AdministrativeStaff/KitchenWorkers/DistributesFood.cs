﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff
{
    public class DistributesFood : WorkerBase
    {
        public DistributesFood(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
        }
    }
}
