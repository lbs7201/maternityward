﻿using MaternityWard.SalaryRank;

namespace MaternityWard
{
    public class HeadOfDepartment : WorkerBase
    {
        public HeadOfDepartment(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new ManagerRank());
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
        }
    }
}
