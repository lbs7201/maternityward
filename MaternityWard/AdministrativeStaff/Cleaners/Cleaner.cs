﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff
{
    public class Cleaner : AdministrativeStaff
    {
        public Cleaner(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
        }
    }
}
