﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff
{
    public class MultiCleaners : AdministrativeStaff
    {
        public MultiCleaners(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new SeniorRank());
        }
    }
}
