﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff
{
    public class ShiftManagerOfCleaners : AdministrativeStaff
    {
        public ShiftManagerOfCleaners(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
        }
    }
}
