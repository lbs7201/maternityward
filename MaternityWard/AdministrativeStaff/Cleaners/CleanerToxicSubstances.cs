﻿using MaternityWard.SalaryRank;

namespace MaternityWard.AdministrativeStaff
{
    public class CleanerToxicSubstancesFactory : AdministrativeStaff
    {
        public CleanerToxicSubstancesFactory(double hourAmount, string name)
        {
            Name = name;
            HoursAmount = hourAmount;
            Ranks.Add(new JuniorRank());
            Ranks.Add(new MakesDecisionsRank(HoursAmount));
            Ranks.Add(new WorkerAtRisk(WagesRanks.TWENTY_PERCENTAGE_OF_WAGE_INCREASE_TO_DEGREE_OF_RISK));
        }
    }
}
