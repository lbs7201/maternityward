﻿using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class HeadOfDepartmentFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new HeadOfDepartment(hourAmount, name);
        }
    }
}
