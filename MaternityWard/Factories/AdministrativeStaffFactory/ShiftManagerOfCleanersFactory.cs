﻿using MaternityWard.AdministrativeStaff;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class ShiftManagerOfCleanersFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new ShiftManagerOfCleaners(hourAmount, name);
        }
    }
}
