﻿using MaternityWard.AdministrativeStaff;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class MultiCleanersFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new MultiCleaners(hourAmount, name);
        }
    }
}
