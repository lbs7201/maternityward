﻿using MaternityWard.AdministrativeStaff.KitchenWorkers;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class ChefFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new Chef(hourAmount, name);
        }
    }
}
