﻿using MaternityWard.AdministrativeStaff.KitchenWorkers;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class CookerFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new Cooker(hourAmount, name);
        }
    }
}
