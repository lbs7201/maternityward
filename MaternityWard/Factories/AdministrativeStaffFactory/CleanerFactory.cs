﻿using MaternityWard.AdministrativeStaff;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class CleanerFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new Cleaner(hourAmount, name); 
        }
    }
}
