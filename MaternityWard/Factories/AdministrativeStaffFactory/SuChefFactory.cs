﻿using MaternityWard.AdministrativeStaff.KitchenWorkers;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class SuChefFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new SuChef(hourAmount, name);
        }
    }
}
