﻿using MaternityWard.AdministrativeStaff;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class CleanerTToxicSubstancesFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new CleanerToxicSubstancesFactory(hourAmount, name);
        }
    }
}
