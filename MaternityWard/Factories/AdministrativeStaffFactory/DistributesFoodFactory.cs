﻿using MaternityWard.AdministrativeStaff;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    class DistributesFoodFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new DistributesFood(hourAmount, name);
        }
    }
}
