﻿using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories
{
    public class DoctorFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new Doctor(hourAmount, name);
        }
    }
}
