﻿using MaternityWard.ProffesionalWorkers.Internship;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories.ProffesionalWorkersFactory
{
    public class BreechInternshipFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new BreechInternship(hourAmount, name);
        }
    }
}
