﻿using MaternityWard.WardRanks.Base;
namespace MaternityWard.Factories.ProffesionalWorkersFactory
{
    public class DepartmentManagerFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new DepartmentManager(hourAmount, name);
        }
     }
}
