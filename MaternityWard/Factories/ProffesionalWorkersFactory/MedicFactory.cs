﻿using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories.ProffesionalWorkersFactory
{
    public class MedicFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new Medic(hourAmount, name);
        }
    }
}
