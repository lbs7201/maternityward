﻿using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories.ProffesionalWorkersFactory
{
    public class BreechMidwifeFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new BreechMidwife(hourAmount, name);
        }
    }
}
