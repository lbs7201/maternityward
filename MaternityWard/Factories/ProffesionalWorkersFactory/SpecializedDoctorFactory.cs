﻿using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories.ProffesionalWorkersFactory
{
    public class SpecializedDoctorFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new SpecializedDoctor(hourAmount, name);
        }
    }
}
