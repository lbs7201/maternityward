﻿using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories.ProffesionalWorkersFactory
{
    public class DeputyHeadOfDepartmentFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new DeputyHeadOfDepartment(hourAmount, name);
        }
    }
}
