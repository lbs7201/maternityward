﻿using MaternityWard.ProffesionalWorkers;
using MaternityWard.WardRanks.Base;

namespace MaternityWard.Factories.ProffesionalWorkersFactory
{
    public class ParamedicFactory : IWorkerFactory
    {
        public WorkerBase CreateWorker(double hourAmount, string name)
        {
            return new Paramedic(hourAmount, name);
        }
    }
}
