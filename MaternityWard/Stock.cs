﻿using MaternityWard.AdministrativeStaff;
using System.Collections.Generic;

namespace MaternityWard
{
    public class Stock
    {
        public List<WorkerBase> workers = new List<WorkerBase>
        {
            new Cleaner(8, "Alex"), 
            new Doctor(10, "Bob"), 
            new HeadNurse(6, "Or"),
            new HeadOfDepartment(21,"Linoy Ben-Shitrit"),
            new SpecializedDoctor(3, "Daniel Vaknin")
        };
    }
}
